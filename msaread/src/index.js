const express = require('express')
const chalk = require('chalk')

const config = require('../config')
const logger = require('./core/logger')

const app = express()

const DEFAULT_PATH = '/api/write'

app.get(`${DEFAULT_PATH}/`, (req, res) => res.json({ res: 'Hello Server' }))
app.get(`${DEFAULT_PATH}/hello`, (req, res) => res.send({res: 'Hello World'}))

app.listen(config.PORT, () => {
  logger.info(`Server running at port: ${chalk.green(config.PORT)}`)
})
