const express = require('express')
const webpack = require('webpack')
const app = express()

const PORT = 5000

const webpackConfig = require('../webpack.config')
const compiler = webpack(webpackConfig)

app.listen(PORT, () => {
  console.log(`Server running at port: ${PORT}`)
})

app.use(require('webpack-dev-middleware')(compiler, {
  stats: {
    colors: true
  }
}))
app.use(require('webpack-hot-middleware')(compiler))
