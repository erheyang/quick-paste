# Prerequisites
1. Docker
2. Docker Compose

# How To Start
```bash
sudo docker-compose build
sudo docker-compose up
```